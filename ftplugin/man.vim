" vimrc
setlocal ts=8
setlocal nolist
setlocal nonumber norelativenumber
setlocal readonly
setlocal buftype=nofile
setlocal bufhidden=hide
setlocal noswapfile
setlocal nomodifiable
setlocal laststatus=0

nnoremap q :q<CR>
nnoremap i <nop>
nnoremap s <nop>
nnoremap a <nop>

augroup ManStatusLine
autocmd!
   autocmd CmdlineEnter * redrawstatus
augroup end
