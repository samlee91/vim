
setlocal nonumber
setlocal norelativenumber
setlocal foldcolumn=2
setlocal wrap linebreak nolist breakindent
setlocal formatoptions=l
setlocal spell

nnoremap <silent> <leader>r :w <Bar> :!Rscript -e "rmarkdown::render('"%"')"<CR>
