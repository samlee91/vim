
syn keyword picomBool    true false
syn match   picomComment "^#.*$"
syn region  picomString  start=+"+ end=+"+ end=+$+ contains=@Spell
syn match   picomNumber  "[0-9\.]\d*"
syn keyword picomParam   transition length pow x y w h no scale down spawn center 
                       \ corner corners rounded radius exclude borders shadow red 
                       \ blue green offset fading fade in out step delta active 
                       \ inactive focus focused opacity size round 
                       \ blur method strength background frame fixed kern kernel 
                       \ experimental backends backend vsync mark wmwin detect 
                       \ ovredir  client transient leader log level wintypes

syn keyword picomWintypes   unknown desktop dock toolbar menu utility splash dialog 
                          \ normal tooltip dropdown_menu popup_menu notification 
                          \ combo dnd
   


hi def link picomComment  Comment
hi def link picomBool     Macro
hi def link picomString   String
hi def link picomNumber   Number
hi def link picomParam    Function
hi def link picomWintypes Macro
