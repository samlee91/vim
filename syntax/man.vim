" Vim syntax file
" Language:	  man
" Maintainer: squidlush



if exists("b:current_syntax")
  finish
endif


syn match  manHeading        "^\a.*$"
syn match  manHeadingOption  "\[\<[A-Z]\+\>\]"
syn match  manOption         "-\a\(\A\)\@="
syn match  manLongOption     "--[a-z0-9-]*"
syn match  manOptionValue    "\(=\)\@<=\a*"
syn match  manCommand        "^\s\{7\}\a*$"
syn match  manEnviVar        "\$\([A-Z]_*\)*"
syn match  manStringSubst    "<\w*>"
syn region manDescription    start="("  end=")" contains=manOption,manLongOption,manOptionValue,manEnviVar


hi def link manHeading        BoldMagenta
hi def link manHeadingOption  BoldBlue
hi def link manOption         BoldBlue
hi def link manLongOption     BoldCyan
hi def link manOptionValue    BoldYellow
hi def link manDescription    BoldGreen
hi def link manCommand        BoldCyan
hi def link manStringSubst    BoldGreen
hi def link manEnviVar        PreProc


let b:current_syntax = "man"

