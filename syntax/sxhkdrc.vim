if exists("b:current_syntax")
    finish
endif

syntax include @Shell syntax/zsh.vim

syn match swComment "^#.*$"
syn match swHotkey "[^ #].*" contains=swKeysym,swModifier,swHotkeySep,swSequence, swInlineComment
syn match swCommand "\v^\s+.*$" containedin=ALL contains=@Shell,swSequenceShell
syn keyword swSuper super
syn keyword swModifier hyper meta alt control ctrl shift mode_switch lock mod1 mod2 mod3 mod4 mod5 any contained
syn match swKeysym "[^ :;{,}+-]\+" contained contains=swAction
syn match swAction "[@~/]" contained
syn match swHotkeySep "[;:+]" contained
syn match swSequenceSep "[,-]" contained
syn match swImport "^include" contained
syn match swImportPath "\v^<include>.*" contains=swImport
syn region swSequence matchgroup=swBrace start=/{/ end=/}/ contained keepend oneline contains=swKeysym,swModifier,swHotkeySep,swSequenceSep,swEnviVar
syn region swSequenceShell matchgroup=swBrace start=/{/ end=/}/ contained keepend oneline contains=swKeysym,swModifier,swHotkeySep,swSequenceSep,swEnviVar
syn region swInlineComment start="#" end="$" contained
syn match swEnviVar "\$\([A-Z]*_*\)*"

hi def link swEnviVar       PreProc
hi def link swComment       Comment
hi def link swInlineComment Comment
hi def link swSuper         Conditional
hi def link swModifier      Function
hi def link swKeysym        Macro
hi def link swAction        Special
hi def link swBrace         Conditional
hi def link swHotkeySep     Delimiter
hi def link swSequenceSep   Delimiter
hi def link swImport        type
hi def link swImportPath    String

let b:current_syntax = "sxhkdrc"
