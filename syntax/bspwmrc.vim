" Vim syntax file
" Language:	        bspwmrc
" Current Maintainer:	Sam Lee


" quit when a syntax file was already loaded
if exists("b:current_syntax")
  finish
endif


" Read the shell syntax to start with
runtime! syntax/zsh.vim



syntax keyword  bspwmBspc      bspc
syntax keyword  bspwmBoolean   true false
syntax match    bspwmCommand   "\(bspc \)\@<=\<\a*\>"
syntax match    bspwmParam     "\(bspc \a* \)\@<=\<\w*\>" 
syntax match    bspwmValue     "\(bspc \a* \w* \s*\)\@<=\<\w*\>"
syntax match    bspwmNumber    "\d"

hi def link bspwmBspc        Conditional
hi def link bspwmCommand     Function
hi def link bspwmParam       Macro
hi def link bspwmNumber      Number
hi def link bspwmBoolean     Number

let b:current_syntax = "bspwmrc"

" vim: ts=8
