" Vim syntax file
" Language:               Configuration File (ini file) for MSDOS/MS Windows
" Version:                2.2
" Original Author:        Sean M. McKee <mckee@misslink.net>
" Previous Maintainer:    Nima Talebi <nima@it.net.au>
" Current Maintainer:     Sam Lee
" Homepage:               http://www.vim.org/scripts/script.php?script_id=3747
" Repository:             https://github.com/xuhdev/syntax-dosini.vim
" Last Change:            2018 Sep 11


" quit when a syntax file was already loaded
if exists("b:current_syntax")
  finish
endif

" shut case off
syn case ignore

syn match  dosiniEquals   "="
syn match  dosiniLabel    "^.\{-}\ze\s*=" nextgroup=dosiniNumber,dosiniValue
syn match  dosiniValue    "\(=\s\)\@<=\<\a\w*\>*$"
syn match  dosiniNumber   "=\zs\s*\d\+\s*$"
syn match  dosiniNumber   "=\zs\s*\d*\.\d\+\s*$"
syn match  dosiniNumber   "\(=\s\)\@<=\d*$"
syn match  dosiniComment  "^[#;].*$"

syn region dosiniHeader   start="^\s*\[" end="\]"
syn region dosiniString   start=+\"+ end=+\"+

" Define the default highlighting.
" Only when an item doesn't have highlighting yet

hi def link dosiniEquals   Conditional
hi def link dosiniNumber   Number
hi def link dosiniHeader   Function
hi def link dosiniComment  Comment
hi def link dosiniLabel    Macro
hi def link dosiniValue    Type
hi def link dosiniString   String


let b:current_syntax = "dosini"

" vim: sts=2 sw=2 et
