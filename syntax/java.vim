" Vim syntax file
" Language:	Java
" Maintainer:	squidlush; Claudio Fleiner <claudio@fleiner.com>
" Last Change:	2018 July 26


if !exists("main_syntax")
  if exists("b:current_syntax")
    finish
  endif
  let main_syntax='java'
  syn region javaFold start="{" end="}" transparent fold
endif

let s:cpo_save = &cpo
set cpo&vim

" some characters that cannot be in a java program (outside a string)
syn match javaError "[\\@`]"
syn match javaError "<<<\|\.\.\|=>\|||=\|&&=\|\*\/"
syn match javaOK "\.\.\."

" use separate name so that it can be deleted in javacc.vim
syn match   javaError2 "#\|=<"
hi def link javaError2 javaError






" keyword definitions
syn keyword javaExternal	native package
syn match   javaExternal        "\<import\>\(\s\+static\>\)\?"
syn keyword javaError		goto const
syn keyword javaConditional	if else switch
syn keyword javaRepeat		while for do
syn keyword javaBoolean		true false
syn keyword javaConstant	null
syn keyword javaTypedef		this super
syn keyword javaOperator	var new instanceof
syn keyword javaType		boolean char byte short int long float double void
syn keyword javaStatement	return
syn keyword javaStorageClass	static synchronized transient volatile final 
                              \ strictfp serializable
syn keyword javaExceptions	throw try catch finally
syn keyword javaAssert		assert
syn keyword javaMethodDecl	synchronized throws
syn keyword javaClassDecl	extends implements interface
syn match   javaTypedef		"\.\s*\<class\>"ms=s+1
syn keyword javaClassDecl	enum
syn match   javaClassDecl	"^class\>"
syn match   javaClassDecl	"[^.]\s*\<class\>"ms=s+1
syn match   javaAnnotation	"@\([_$a-zA-Z][_$a-zA-Z0-9]*\.\)*[_$a-zA-Z][_$a-zA-Z0-9]*\>"
                               \ contains=javaString
syn match   javaClassDecl	"@interface\>"
syn keyword javaBranch		break continue nextgroup=javaUserLabelRef skipwhite
syn match   javaUserLabelRef	"\k\+" contained
syn match   javaVarArg		"\.\.\."
syn keyword javaScopeDecl	public protected private abstract


if fnamemodify(bufname("%"), ":t") == "module-info.java"
    syn keyword javaModuleStorageClass	module transitive
    syn keyword javaModuleStmt		open requires exports opens uses provides
    syn keyword javaModuleExternal	to with
    syn cluster javaTop add=javaModuleStorageClass,javaModuleStmt,javaModuleExternal
endif


syn cluster javaTop add=javaC_JavaLang
syn cluster javaClasses add=javaC_JavaLang
syn match javaLangClass "\<System\>"
syn cluster javaTop add=javaR_JavaLang
syn cluster javaClasses add=javaR_JavaLang
hi def link javaR_JavaLang javaR_Java
syn keyword javaR_JavaLang    NegativeArraySizeException ArrayStoreException 
                            \ IllegalStateException RuntimeException IndexOutOfBoundsException
                            \ UnsupportedOperationException ArrayIndexOutOfBoundsException
                            \ ArithmeticException ClassCastException EnumConstantNotPresentException
                            \ StringIndexOutOfBoundsException IllegalArgumentException
                            \ IllegalMonitorStateException IllegalThreadStateException
                            \ NumberFormatException NullPointerException 
                            \ TypeNotPresentException SecurityException
syn keyword javaC_JavaLang    Process RuntimePermission StringKeySet CharacterData01 
                            \ Class ThreadLocal ThreadLocalMap CharacterData0E Package
                            \ Character StringCoding Long ProcessImpl ProcessEnvironment 
                            \ Short AssertionStatusDirectives 1PackageInfoProxy 
                            \ UnicodeBlock InheritableThreadLocal AbstractStringBuilder 
                            \ StringEnvironment ClassLoader ConditionalSpecialCasing 
                            \ CharacterDataPrivateUse StringBuffer StringDecoder Entry 
                            \ StringEntry WrappedHook StringBuilder StrictMath State 
                            \ ThreadGroup Runtime CharacterData02 MethodArray Object 
                            \ CharacterDataUndefined Integer Gate Boolean Enum Variable 
                            \ Subset StringEncoder Void Terminator CharsetSD 
                            \ IntegerCache CharacterCache Byte CharsetSE Thread 
                            \ SystemClassLoaderAction CharacterDataLatin1 StringValues 
                            \ StackTraceElement Shutdown ShortCache String ConverterSD 
                            \ ByteCache Lock EnclosingMethodInfo Math Float Value 
                            \ Double SecurityManager LongCache ProcessBuilder 
                            \ StringEntrySet Compiler Number UNIXProcess ConverterSE 
                            \ ExternalData CaseInsensitiveComparator CharacterData00 
                            \ NativeLibrary
syn keyword javaE_JavaLang    IncompatibleClassChangeError InternalError UnknownError 
                            \ ClassCircularityError AssertionError ThreadDeath 
                            \ IllegalAccessError NoClassDefFoundError ClassFormatError 
                            \ UnsupportedClassVersionError NoSuchFieldError VerifyError 
                            \ ExceptionInInitializerError InstantiationError LinkageError 
                            \ NoSuchMethodError Error UnsatisfiedLinkError StackOverflowError 
                            \ AbstractMethodError VirtualMachineError OutOfMemoryError
syn keyword javaX_JavaLang   CloneNotSupportedException Exception NoSuchMethodException 
                           \ IllegalAccessException NoSuchFieldException Throwable 
                           \ InterruptedException ClassNotFoundException InstantiationException
syn cluster javaTop        add=javaX_JavaLang
syn cluster javaClasses    add=javaX_JavaLang



hi def link javaX_JavaLang javaX_Java
hi def link javaE_JavaLang javaE_Java
hi def link javaR_Java     javaR_
hi def link javaC_Java     javaC_
hi def link javaE_Java     javaE_
hi def link javaX_Java     javaX_
hi def link javaX_	   javaExceptions
hi def link javaR_         javaExceptions
hi def link javaE_         javaExceptions
hi def link javaC_         Macro



syn keyword javaLangObject clone equals finalize getClass hashCode
syn keyword javaLangObject notify notifyAll toString wait
syn cluster javaTop add=javaLangObject


if filereadable(expand("<sfile>:p:h")."/javaid.vim")
  source <sfile>:p:h/javaid.vim
endif

if exists("java_space_errors")
  if !exists("java_no_trail_space_error")
    syn match	javaSpaceError	"\s\+$"
  endif
  if !exists("java_no_tab_space_error")
    syn match	javaSpaceError	" \+\t"me=e-1
  endif
endif



syn match   javaUserLabel     "^\s*[_$a-zA-Z][_$a-zA-Z0-9_]*\s*:"he=e-1 contains=javaLabel
syn keyword javaLabel	      default
syn region  javaLabelRegion   transparent matchgroup=javaLabel start="\<case\>" matchgroup=NONE end=":" 
                            \ contains=javaNumber,javaCharacter,javaString
syn cluster javaTop           add=javaExternal,javaError,javaError,javaBranch,
                            \ javaLabelRegion,javaLabel,javaConditional,javaRepeat,
                            \ javaBoolean,javaConstant,javaTypedef,javaOperator,javaType,
                            \ javaType,javaStatement,javaStorageClass,javaAssert,
                            \ javaExceptions,javaMethodDecl,javaClassDecl,javaClassDecl,
                            \ javaClassDecl,javaScopeDecl,javaError,javaError2,javaUserLabel,
                            \ javaLangObject,javaAnnotation,javaVarArg


" Comments
syn keyword javaTodo		 contained TODO FIXME XXX
if exists("java_comment_strings")
  syn region  javaCommentString    contained start=+"+ end=+"+ end=+$+ end=+\*/+me=s-1,he=s-1 contains=javaSpecial,javaCommentStar,javaSpecialChar,@Spell
  syn region  javaComment2String   contained start=+"+	end=+$\|"+  contains=javaSpecial,javaSpecialChar,@Spell
  syn match   javaCommentCharacter contained "'\\[^']\{1,6\}'" contains=javaSpecialChar
  syn match   javaCommentCharacter contained "'\\''" contains=javaSpecialChar
  syn match   javaCommentCharacter contained "'[^\\]'"
  syn cluster javaCommentSpecial add=javaCommentString,javaCommentCharacter,javaNumber
  syn cluster javaCommentSpecial2 add=javaComment2String,javaCommentCharacter,javaNumber
endif


syn region  javaComment		 start="/\*"  end="\*/" contains=@javaCommentSpecial,javaTodo,@Spell
syn match   javaCommentStar	 contained "^\s*\*[^/]"me=e-1
syn match   javaCommentStar	 contained "^\s*\*$"
syn match   javaLineComment	 "//.*" contains=@javaCommentSpecial2,javaTodo,@Spell


hi def link javaCommentString javaString
hi def link javaComment2String javaString
hi def link javaCommentCharacter javaCharacter

syn cluster javaTop add=javaComment,javaLineComment

if !exists("java_ignore_javadoc") && main_syntax != 'jsp'
  syntax case ignore
  syntax spell default
  syn region  javaDocComment	start="/\*\*"  end="\*/" keepend contains=javaCommentTitle,@javaHtml,javaDocTags,javaDocSeeTag,javaTodo,@Spell
  syn region  javaCommentTitle	contained matchgroup=javaDocComment start="/\*\*"   matchgroup=javaCommentTitle keepend end="\.$" end="\.[ \t\r<&]"me=e-1 end="[^{]@"me=s-2,he=s-1 end="\*/"me=s-1,he=s-1 contains=@javaHtml,javaCommentStar,javaTodo,@Spell,javaDocTags,javaDocSeeTag

  syn region javaDocTags	 contained start="{@\(code\|link\|linkplain\|inherit[Dd]oc\|doc[rR]oot\|value\)" end="}"
  syn match  javaDocTags	 contained "@\(param\|exception\|throws\|since\)\s\+\S\+" contains=javaDocParam
  syn match  javaDocParam	 contained "\s\S\+"
  syn match  javaDocTags	 contained "@\(version\|author\|return\|deprecated\|serial\|serialField\|serialData\)\>"
  syn region javaDocSeeTag	 contained matchgroup=javaDocTags start="@see\s\+" matchgroup=NONE end="\_."re=e-1 contains=javaDocSeeTagParam
  syn match  javaDocSeeTagParam  contained @"\_[^"]\+"\|<a\s\+\_.\{-}</a>\|\(\k\|\.\)*\(#\k\+\((\_[^)]\+)\)\=\)\=@ extend
  syntax case match
endif

" match the special comment /**/
syn match   javaComment		 "/\*\*/"

" Strings and constants
syn match   javaSpecialError	 contained "\\."
syn match   javaSpecialCharError contained "[^']"
syn match   javaSpecialChar	 contained "\\\([4-9]\d\|[0-3]\d\d\|[\"\\'ntbrf]\|u\x\{4\}\)"
syn region  javaString		start=+"+ end=+"+ end=+$+ contains=@Spell
syn match   javaCharacter	 "'[^']*'" contains=javaSpecialChar,javaSpecialCharError
syn match   javaCharacter	 "'\\''" contains=javaSpecialChar
syn match   javaCharacter	 "'[^\\]'"
syn match   javaNumber		 "\<\(0[bB][0-1]\+\|0[0-7]*\|0[xX]\x\+\|\d\(\d\|_\d\)*\)[lL]\=\>"
syn match   javaNumber		 "\(\<\d\(\d\|_\d\)*\.\(\d\(\d\|_\d\)*\)\=\|\.\d\(\d\|_\d\)*\)\([eE][-+]\=\d\(\d\|_\d\)*\)\=[fFdD]\="
syn match   javaNumber		 "\<\d\(\d\|_\d\)*[eE][-+]\=\d\(\d\|_\d\)*[fFdD]\=\>"
syn match   javaNumber		 "\<\d\(\d\|_\d\)*\([eE][-+]\=\d\(\d\|_\d\)*\)\=[fFdD]\>"

" unicode characters
syn match   javaSpecial "\\u\d\{4\}"
syn cluster javaTop     add=javaString,javaCharacter,javaNumber,javaSpecial,javaStringError

hi def link javaParenError	javaError

if exists("java_highlight_functions")
   syn match javaLambdaDef "([a-zA-Z0-9_<>\[\], \t]*)\s*->"
   " needs to be defined after the parenthesis error catcher to work
endif

if !exists("java_minlines")
  let java_minlines = 10
endif
exec "syn sync ccomment javaComment minlines=" . java_minlines

" The default highlighting.



" my additions
syn match javaClass     "\(class\s\)\@<=[A-Za-z]*"
syn match javaObject    "[A-Z]\w*\(\.\)\@="
syn match javaObject    "[A-Za-z]\w*\(\s\w*\s=\)\@="
syn match javaObject    "[A-Za-z]\w*\(\s\w*;\)\@="
syn match javaObject    "[A-Za-z]\w*\(<\)\@="
syn match javaFunc      "[A-za-z]\w*\((\)\@="
syn match javaSubType   "\(<\)\@<=[A-Z]\w*"
syn keyword javaSubType Type E
syn match javaLambda    "[a-zA-Z_][a-zA-Z0-9_]*\s*->"

syn match javaMathOps "\v\="
syn match javaMathOps "\v\+"
syn match javaMathOps "\v-"
syn match javaMathOps "\v\*"
syn match javaMathOps "\v\^"
syn match javaMathOps "\v\&"
syn match javaMathOps "\v\&\&"
syn match javaMathOps "\v\|"
syn match javaMathOps "\v\!"
syn match javaMathOps "\v\s\<\s"
syn match javaMathOps "\v\s\>\s"

hi def link javaMathOps     Conditional
hi def link javaFunc        Function
hi def link javaObject      Type
hi def link javaClassDecl   Conditional
hi def link javaClass       Type
hi def link javaConstant    javaNumber
hi def link javaLangObject  Function
hi def link javaSubType     Macro
hi def link javaLambda      Function
" end my additions







hi def link javaC_JavaLang javaC_Java
hi def link javaStorageClass   Conditional
hi def link javaBinaryOps      Conditional 
hi def link javaLambdaDef		Function
hi def link javaFuncDef		Function
hi def link javaVarArg			Function
hi def link javaBraces			Function
hi def link javaBranch			Conditional
hi def link javaUserLabelRef		javaUserLabel
hi def link javaLabel			Label
hi def link javaUserLabel		Label
hi def link javaConditional		Conditional
hi def link javaRepeat			Repeat
hi def link javaExceptions		Exception
hi def link javaAssert			Statement
hi def link javaMethodDecl		StorageClass
hi def link javaScopeDecl		Conditional

hi def link javaBoolean		Boolean
hi def link javaSpecial		Special
hi def link javaSpecialError		Error
hi def link javaSpecialCharError	Error
hi def link javaString			String
hi def link javaCharacter		Character
hi def link javaSpecialChar		SpecialChar
hi def link javaNumber			Number
hi def link javaError			Error
hi def link javaStringError		Error
hi def link javaStatement		Statement
hi def link javaOperator		Operator
hi def link javaComment		Comment
hi def link javaDocComment		Comment
hi def link javaLineComment		Comment
hi def link javaTypedef		Typedef
hi def link javaTodo			Todo
hi def link javaAnnotation		PreProc

hi def link javaCommentTitle		Comment
hi def link javaDocTags		Special
hi def link javaDocParam		Function
hi def link javaDocSeeTagParam		Function
hi def link javaCommentStar		javaComment

hi def link javaType			Type
hi def link javaExternal	Include

hi def link htmlComment	   	Special
hi def link htmlCommentPart   Special
hi def link javaSpaceError		Error

if fnamemodify(bufname("%"), ":t") == "module-info.java"
    hi def link javaModuleStorageClass	StorageClass
    hi def link javaModuleStmt		Statement
    hi def link javaModuleExternal	Include
endif

let b:current_syntax = "java"

if main_syntax == 'java'
  unlet main_syntax
endif

let b:spell_options="contained"
let &cpo = s:cpo_save
unlet s:cpo_save

" vim: ts=8
