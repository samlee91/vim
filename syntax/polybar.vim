" Vim syntax file
" Language:	Polybar Config
" Current Maintainer:	Sam Lee


" quit when a syntax file was already loaded
if exists("b:current_syntax")
  finish
endif


" Read the dosini syntax to start with
runtime! syntax/dosini.vim
"unlet b:current_syntax



syntax keyword  polybarEnv        env
syntax region   polybarEnviVar    start=+\${+ end=+}+ contains=polybarEnv
syntax match    polybarModules    "\(modules-\<.*\> = \)\@<=.*$" contains=polybarEnviVar


hi def link polybarEnv       Error
hi def link polybarEnviVar   PreProc
hi def link polybarModules   dosiniValue

let b:current_syntax = "polybar"

" vim: ts=8
