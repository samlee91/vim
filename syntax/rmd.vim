
if exists("b:current_syntax")
  finish
endif


let s:cpo_save = &cpo
set cpo&vim


syntax match rmdPreamble      "^---\(\n.*\)*---\s*$"  contains=@NoSpell,rmdLaTexKeyword,rmdNumber,rmdLaTexRegion
syntax match rmdLargeHeader   "^#\s.*$"               contains=@Spell,@LaTexBlock
syntax match rmdMediumHeader  "^##\s.*$"              contains=@Spell,@LaTexBlock
syntax match rmdSmallHeader   "^###\s.*$"             contains=@Spell,@LaTexBlock



syntax match rmdBullet        "^\s*\* "
syntax match rmdDash          "^\s*- "
syntax match rmdNumber        "\d" 

syntax region rmdItalic        start="\*"     end="\*"     contained contains=@Spell
syntax region rmdBold          start="\*\*"   end="\*\*"   contained contains=@Spell
syntax region rmdBoldItalic    start="\*\*\*" end="\*\*\*" contained contains=@Spell


" LaTeX 
syntax include @LaTexBlock syntax/tex.vim
syntax match    rmdLaTexKeyword  "\\\a\a*\(\s*\)\@="
"syntax region   rmdLaTexBlock   start="\\begin" end="\\end"  contains=@LaTexBlock
"syntax region  rmdInlineMath   start=+\$+ end=+\$+           contains=@LaTexBlock
"syntax region  rmdLaTexBlock   start="^\\begin" end="\\end"  contains=@LaTexBlock
"syntax match   rmdLaTexRegion  "\(\\\<\a*\>\)\@<={.*}"       contains=@LaTexBlock



" Syntax highlighting for code blocks
unlet b:current_syntax
let g:markdown_fenced_languages = ['cpp', 'python', 'sh', 'zsh', 'java', 'julia']
syntax include @CodeBlock syntax/markdown.vim
syntax region rmdCodeBlock   start="```"   end="```"   contains=@CodeBlock


" cterm/bold text
execute "highlight rmdLargeHeader  cterm=bold          guifg="$BLUE 
execute "highlight rmdMediumHeader cterm=bold          guifg="$MAGENTA
execute "highlight rmdSmallHeader  cterm=bold          guifg="$CYAN
execute "highlight rmdMathModeChar cterm=bold          guifg="$YELLOW
execute "highlight rmdBullet       cterm=bold          guifg="$WHITE
execute "highlight rmdDash         cterm=bold          guifg="$WHITE

"highlight   rmdBold          cterm=bold
"highlight   rmdItalic        cterm=italic
"highlight   rmdBoldItalic    cterm=bold,italic
hi def link rmdNumber        Number
hi def link rmdPreamble      String
hi def link rmdLaTexBlock    Conditional
hi def link rmdLaTexKeyword  Function
hi def link rmdLaTexRegion   PreProc


let b:current_syntax = "rmd"
let &cpo = s:cpo_save
unlet s:cpo_save
