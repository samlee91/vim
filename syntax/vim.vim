


syn match vimDeref  "&" nextgroup=vimIsCommand


hi def link vimFunction  Function
hi def link vimUserFunc  Function
hi def link vimIsCommand vimVar
hi def link vimDeref     vimVar
