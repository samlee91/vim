
" leader key
nnoremap <Space> <Nop>
let mapleader = " "
set ttimeoutlen=10



" source/edit vimrc or current file
nnoremap <leader>v  :tabnew $VIMRC<CR>
nnoremap <leader>so :source %<CR>
nnoremap <silent> <leader>sv :call Refresh()<CR>

" splits
nnoremap <leader>o :vsplit 
nnoremap <leader>h <C-w>h
nnoremap <leader>j <C-w>j
nnoremap <leader>k <C-w>k
nnoremap <leader>l <C-w>l

" search and replace
nnoremap <leader>sr :%s//g<left><left>

" yanking and clipboard
nnoremap Y y$

" end of line
noremap E $

" keep centered while searching
nnoremap n nzzzv
nnoremap N Nzzzv


" scrolling
nnoremap <S-k> {
vnoremap <S-k> {
nnoremap <S-j> }
vnoremap <S-j> }

" concatenate lines
nnoremap <C-j> J

" insert a new line in normal mode
nnoremap <leader><CR> O<ESC>

" toggle line numbers
nnoremap <silent> <leader>n :call ToggleLineNumbers()<CR>


" highlight groups
nnoremap <leader>sg :call SynGroup()<CR>
nnoremap <leader>sh :source $VIMRUNTIME/syntax/hitest.vim<CR>



" LaTex specific keybindings
augroup MathFiles
   autocmd!
   autocmd Filetype rmd,tex inoremap $ $$<Esc>i
   autocmd Filetype rmd,tex inoremap $$<CR> $$<CR>$$<ESC>O<Tab>
   autocmd Filetype rmd,tex inoremap \[<CR> \[<CR>\]<Esc>O<Tab>
   autocmd Filetype rmd,tex nnoremap j gj
   autocmd Filetype rmd,tex nnoremap k gk
augroup end



