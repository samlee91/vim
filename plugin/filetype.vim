
" map files to filetypes by location/regex
let filetype_map = {
   \ "bspwmrc": "*bspwmrc",
   \ "dosini": "*dunstrc",
   \ "i3config": "*i3/*",
   \ "kitty": "*kitty/*.conf",
   \ "picom": "*picom/*.conf",
   \ "polybar": "*polybar/*.config",
   \ "rasi": "*rofi/*",
   \ "sxhkdrc": "*sxhkdrc*"
   \}


augroup FileTypes
   for [key, value] in items(filetype_map)
      execute "autocmd BufNewFile,BufRead " . value . " set filetype=" . key
   endfor
augroup end
   

 augroup Spelling
    autocmd!
    autocmd BufRead,BufNewFile *.md,*.rmd,*.tex,*.txt setlocal spell
 augroup end
