
let g:wrappers = { '(':')', '{':'}', '[':']', '<':'>'}

for [key, value] in items(g:wrappers)
   exec "vnoremap <silent> <leader>" . key . " :call InsertWraper(\"" . key . "\")<CR>"
   exec "vnoremap <silent> <leader>" . value . " :call InsertWraper(\"" . value . "\")<CR>"
   exec "nnoremap <silent> <leader>" . key . " veh :call InsertWraper(\"" . key . "\")<CR>"
   exec "nnoremap <silent> <leader>" . value . " veh :call InsertWraper(\"" . value . "\")<CR>"
endfor


" wrap single words in quotes from normal mode
nnoremap <leader>" i"<Esc>ea"<Esc>b
nnoremap <leader>' i'<Esc>ea'<Esc>b


function GetWrapperPair(wrapper_char)
   for [key, value] in items(g:wrappers)
      if a:wrapper_char == key
         return [key, value, "start"]
      elseif a:wrapper_char == value
         return [key, value, "end"]
      endif
   endfor
endfunc


function InsertWraper(wrapper_char)
   let start = getpos("'<")[1:2]
   let end = getpos("'>")[1:2]
   let [char_L, char_R, exit_position] = GetWrapperPair(a:wrapper_char)

   :call cursor(start[0], start[1])
   exec "normal! i" . char_L 
   :call cursor(end[0], end[1] + 2)
   exec "normal! i" . char_R . "\<Esc>"

   if exit_position == "start" 
      :call cursor(start[0], start[1])
   endif
endfunc

