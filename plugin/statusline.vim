
let g:show_statusline = 1
let g:bumpers = ['', '']
let g:cursor_char = ''


"     group_name      :  foreground,  background,  style
let g:line_colors = {
   \ 'ModeColor' : [$BACKGROUND, $FOREGROUND_ALT, 'bold'],
   \ 'Separator' : [$FOREGROUND_ALT, $BACKGROUND, 'NONE'],
   \ 'Blank'     : [$BACKGROUND, $BACKGROUND, 'NONE']
   \}

for [key, value] in items(line_colors)
   execute "highlight " . key . " guifg="value[0]
   execute "highlight " . key . " guibg="value[1]
   execute "highlight " . key . " cterm="value[2]
   execute "highlight " . key . " term="value[2]   
endfor



function CursorModule()
   return '(' . line('.')  . ',' . col('.') . ')'
endfunc


function ModeModule()

   let g:mode_map = {
      \ 'n'      : ' Normal',
      \ 'i'      : ' Insert',
      \ 'c'      : ' Command',
      \ "v"      : ' Visual',
      \ "\<C-V>" : ' Visual'
      \}

   for [key, value] in items(g:mode_map)
      "if match(mode(), key) == 0
      if mode() == key
         let g:mode_label = value
      endif
   endfor

   return g:mode_label

endfunc


function Disable_default_statusline()
   set noshowmode
   set noruler
   set laststatus=2
endfunc




if show_statusline == 1

   call Disable_default_statusline()

   set statusline=%#Blank#\ 
   set statusline+=%#Separator#%{g:bumpers[0]}
   set statusline+=%#ModeColor#%{ModeModule()}
   set statusline+=%#Separator#%{g:bumpers[1]}

   set statusline+=%#Blank#%= 
   set statusline+=%#Separator#%{g:bumpers[0]}
   set statusline+=%#ModeColor#%{g:cursor_char}
   set statusline+=%#ModeColor#%{CursorModule()}
   set statusline+=%#Separator#%{g:bumpers[1]}
   set statusline+=\  
endif

