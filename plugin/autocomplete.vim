
let enable_autocomplete = v:true

if enable_autocomplete  

   set completeopt=menu,menuone,noinsert
   
   inoremap <silent> <BS> <BS><C-r>=getline('.')[col('.')-3:col('.')-2]=~#'\k\k'?!pumvisible()?"\<lt>C-n>":'':pumvisible()?"\<lt>C-y>":''<CR>
   "inoremap <silent> <expr> <CR>            pumvisible() ? "\<C-y>" : ""
   inoremap <silent> <expr> <Tab>           pumvisible() ? "\<C-n>" : "\<Tab>"
   inoremap <silent> <expr> <S-Tab>         pumvisible() ? "\<C-p>" : "\<S-Tab>"

   function! s:AutoInsert(char)
     if !pumvisible() && !exists('s:skinny_complete') && getline('.')[col('.') - 2].a:char =~# '\k\k'
       let s:skinny_complete = 1
       noautocmd call feedkeys("\<C-n>", "nt")
     endif
   endfunction
   
   augroup SkinnyAutoComplete
     autocmd!
     autocmd InsertCharPre * call <SID>AutoInsert(v:char)
     autocmd CompleteDone * if exists('s:skinny_complete')
              \ | unlet s:skinny_complete 
              \ | endif
   augroup end

endif
