

let bracket_pairs = {'{':'}', '(':')', '[':']'}


function InEmptyBrackets(left_brack, right_brack)
   return functions#CharAt(-1) == a:left_brack && functions#CharAt(0) == a:right_brack
endfunc



for [key, value] in items(bracket_pairs)
   " auto insert closing bracket
   exec "inoremap " . key . " " . key . value . "<Esc>i"
   " auto format indentation when inside empty brackets on 'Enter' keypress
   exec "inoremap <expr> <CR> InEmptyBrackets('" . key . "','" . value . "') ? \"\<CR>\<Esc>O\" : \"\<CR>\""
   " pad cursor on both sides when inside empty brackets on 'Space' keypress
   exec "inoremap <expr> <Space> InEmptyBrackets('" . key . "','" . value . "') ? \"\<Space>\<Space>\<Esc>i\" : \"\<Space>\""
   " skip closing bracket if already present when typed
   exec "inoremap <expr> " . value . " functions#CharAt(0) == '" . value . "' ? \"\<Esc>la\" : \"" . value . "\""
endfor



