
nnoremap + :call Increment()<CR>


function CharUnderCursor()
   return getline('.')[col('.') - 1]
endfunc


function ToggleTrueFalse(char_under_cursor)
   let text_under_cursor = expand("<cword>")

   if l:text_under_cursor ==# "True"
      return "False"
   elseif l:text_under_cursor ==# "False"
      return "True"
   elseif l:text_under_cursor ==# "true"
      return "false"
   elseif l:text_under_cursor ==# "false"
      return "true"
   else
      return l:text_under_cursor
   endif
endfunc


function Increment()
   let text = CharUnderCursor()

   if match(l:text, "[ft]") == 0
      let new_word = ToggleTrueFalse(l:text)
      exec "call feedkeys('cw" . new_word . "\<Esc>b')"
   elseif match(l:text, "[0123456789]") == 0
      let new_word = l:text + 1
      exec "call feedkeys('r" . new_word . "\<Esc>')"
   endif

endfunc

