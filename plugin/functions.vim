
" Source vimrc and reset filetype
function Refresh()
   let current_filetype = &ft
   source $VIMRC
   exec 'set filetype=' .. current_filetype
endfunc


" Show or hide line numbers
function ToggleLineNumbers()
   if &number
      set nonumber
      set norelativenumber
      set foldcolumn=2
   else
      set number
      set relativenumber
      set foldcolumn-=2
   endif
endfunc


" Show syntax highlighting stack
function SynGroup()
   let sid = synID(line('.'), col('.'), 1)                                       
   let syngroup = synIDattr(sid, 'name')
   let base_syngroup = synIDattr(synIDtrans(sid), 'name')
   echo syngroup .. ' -> ' .. base_syngroup
endfunc


" Return character <distance_from_cursor> units from the cursor (positive or negative)
function functions#CharAt(distance_from_cursor)
   return getline('.')[col('.') + distance_from_cursor - 1]
endfunc

