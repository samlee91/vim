
" Show Tabs
" 0 = never, 1 = one or more file, 2 = always
let g:show_tabline = 0
let LeftSeparator = ''
let RightSeparator = ''


" Colors              :  foreground,   background,  style
let TabColors = {
   \ 'ActiveTab'      : [$FOREGROUND, $BACKGROUND, 'bold'],
   \ 'InactiveTab'    : [$BACKGROUND, $FOREGROUND_ALT, 'bold'],
   \ 'LeftSeparator'  : [$BACKGROUND, $FOREGROUND_ALT, 'none'],
   \ 'RightSeparator' : [$FOREGROUND_ALT, $BACKGROUND, 'none']
   \}


" Set highlight groups
for [key, value] in items(TabColors)
   exec "highlight " . key . " guifg=" . value[0]
   exec "highlight " . key . " guibg=" . value[1]
   exec "highlight " . key . " cterm=" . value[2]
   exec "highlight " . key . " term=" . value[2]
endfor


" Keybindings
nnoremap <C-o> :tabnew 
nnoremap <silent> <C-l> :tabnext<cr>
nnoremap <silent> <C-h> :tabprevious<cr>



function GetFileName(n)

   let buffers = tabpagebuflist(a:n)
   let win_number = tabpagewinnr(a:n)
   return fnamemodify(bufname(buffers[win_number - 1]), ':t')

endfunction



function DrawTabs()

   let num_tabs = tabpagenr('$')
   let tab_line = ''

   let tab_line = ' %#RightSeparator#%{LeftSeparator}' 
              \ . '%#InactiveTab# ' 
              \ . '%#RightSeparator#%{RightSeparator}'

   for i in range(num_tabs)
      if i + 1  == tabpagenr()
         let tab_line .= ' %#ActiveTab#%{GetFileName(' . (i + 1) . ')} '
      else
         let tab_line .= '%#LeftSeparator#%{RightSeparator}'
         let tab_line .= ' %#InactiveTab#%{GetFileName(' . (i + 1) . ')} '
         let tab_line .= '%#RightSeparator#%{RightSeparator}'
      endif
   endfor
   
   let tab_line .= '%#ActiveTab#%T'
   return tab_line 

endfunction



set tabline=
set tabline+=%!DrawTabs()
