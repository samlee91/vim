
" General
let $VIMRC = "~/.vim/vimrc"
set shell=$SHELL
set noerrorbells
set noswapfile
set nowrap
set viminfo+=n~/.vim/.viminfo

" Layout
set foldcolumn=0
set smartindent
set shiftwidth=3
set expandtab
set tabstop=3 
set softtabstop=3
set number relativenumber
set cursorline 
set scrolloff=10
set splitright

" Searching
set incsearch
set ignorecase
set smartcase

" File Types
filetype on
filetype plugin on
filetype indent on

" Theme
syntax on
set termguicolors
set guicursor=
colorscheme systemtheme
